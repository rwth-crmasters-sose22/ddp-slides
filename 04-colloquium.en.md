---
marp: true
---

# DDP-SoSe22 - Class 03
## Pre-colloquium

<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
</style>

---

# Cultural orientation

- _Trust formation_ between people is either **task-based** or **relationship-based**.
- In some cultures, one method is more predominant that the other, this effects the way we subconsciously **acquire** and **solicit** trust from other people. 
- _Your relationship with me is:_
    - **inside of class:** purely _task-based_
    - **outside of class:** purely _relationship-based_

---

# Before we begin

- Game afternoon was a great success, we played a game called _SKYJO_.
- Next game afternoon will be at a neutral location outside of the construction site (a cafe in pontstraße), and we will play a game called _the spirit_. Please join us!

---

# Team formation policy

- Different mother tongues
- Different disciplines

---

# Mother Tongues

- Arabic (x5)
- Bosnian (x1)
- Chinese (x2)
- English (x1)
- German (x3)
- Hindi (x2)
- Persian (x2)
- Portuguese (x1)
- Spanish (x5)
- Turkish (x2)

---

# Disciplines

- Architecture (x9)
- Civil Engineering (x7)
- Computer Science (x2)
- Mechanical Engineering (x5)


