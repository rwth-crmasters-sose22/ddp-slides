import fs from 'fs';

fs.readdirSync('.').forEach((path) => { 
    if (path.endsWith('.md')) 
        fs.writeFileSync(path.replace('.md', '.json'), 
        JSON.stringify(sentencifySlides(path)), 'utf8');  
});

const existingTag = Symbol();
const selfClosingTag = Symbol();
const openingTag = Symbol();
const closingTag = Symbol();
const openingOrClosingTag = Symbol();

const tagTranslationTable = [
    ['^---\s*$',            false,  selfClosingTag,         'slide-splitter'    ], 
    ['<(\w+)>.*?<\/\1>',    true,   existingTag,            ''                  ],
    null,
    ['  $',                 false,  selfClosingTag,         'line-break'        ],
    ['\[',                  false,  openingTag,             'square-bracket'    ],
    ['\]',                  false,  closingTag,             'square-bracket'    ],
    ['```\s*(\w*)\s*',      false,  openingOrClosingTag,    'code-block'        ],
    ['\!',                  false,  selfClosingTag,         'image'             ],
    ['(?<=\])\(([^\)]*)\)', false,  selfClosingTag,         'link'              ],
]

function sentencifySlides(sourceText) {
    let sentences = [];
    let state = { openLink: false }
    for (let i = 0; i <= sourceText.length; i++) {
        let replacementText = sourceText[i];
        let previousChar = sourceText[i - 1];
        switch(sourceText[i]) {
            case '[':
                replacementText = '<square-bracket>';
                break;
            case ']':
                replacementText = '</square-bracket>';
                break;
            case '(':
                if (sourceText[i - 1] === ']') {
                    replacementText = '<link>';
                    state.openLink = true;
                }
                break;
            case ')':
                if (state.openLink === true) {
                    replacementText = '</link>';
                    state.openLink = false;
                }
            
        }
    }
}