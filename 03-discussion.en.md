---
marp: true
---

# DDP-SoSe22 - Class 03
## Discussion

<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
</style>

---

# Before we begin

- We will discuss how to submit the work for last week's DSSIPD assignment
- Please go to the DSSIPD's [class 03 supplementary slides]().