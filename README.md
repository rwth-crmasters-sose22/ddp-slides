# DDP SoSe 2022

## Course slides

The slides of this course are generated from the Markdown (i.e. `.md`) files in this repository.

This means that changing the `.md` files will result in the slides changing automatically.

This magic is performed using the recipe in the [`.gitlab-ci.yml`](./.gitlab-ci.yml), but more on that later in the course.

## List of presentations

> _Please note that the German, Chinese and Spanish slides are still at an experimental stage!_

1. Concept finding slides:
    - English [HTML](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/01-concept-finding.en.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/01-concept-finding.en.pdf).
    - Deutsch (automatisch übersetzt mit DeepL): [HTML](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/01-concept-finding.de.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/01-concept-finding.de.html).
    - 中文（使用 DeepL 自动翻译): [HTML](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/01-concept-finding.zh.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/01-concept-finding.zh.pdf).
    - Español (traducido automáticamente con DeepL): [HTML](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/01-concept-finding.es.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/01-concept-finding.es.pdf).
1. Marketplace idea mapping slides:
    - English [HTML](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/02-mind-mapping.en.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/02-mind-mapping.en.pdf).
    - Deutsch (automatisch übersetzt mit DeepL): [HTML](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/02-mind-mapping.de.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/02-mind-mapping.de.html).
    - 中文（使用 DeepL 自动翻译): [HTML](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/02-mind-mapping.zh.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/02-mind-mapping.zh.pdf).
    - Español (traducido automáticamente con DeepL): [HTML](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/02-mind-mapping.es.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/02-mind-mapping.es.pdf).
