const languages = ['de', 'zh', 'es'];

import * as deepl from 'deepl-node';
import fs from 'fs';
import path from 'path';

// DEEPL_AUTHKEY is an environmental variable for the API key
const authKey = process.env.DEEPL_AUTHKEY;
const translator = new deepl.Translator(authKey);

// Usage: node i18n.mjs [source_file]
const args = process.argv.slice(2);

await (async () => {
    try {
        if (fs.existsSync(args[0]) &&
            fs.lstatSync(args[0]).isFile() &&
            args[0].endsWith('.en.md')) {

            const translateText = fs.readFileSync(args[0], 'utf8', 'r');

            languages.forEach((lang) => {
                translator.translateText(translateText, 'en', lang,
                    {
                        preserveFormatting: true,
                        tagHandling: 'xml',
                        ignoreTags: ['style', 'ignore']
                    }).then((result) => {
                    const translatedFile = path.dirname(args[0]) + '/i18n/' + 
                        path.basename(args[0]).replace('.en.', '.' + lang + '.');
                    if (fs.existsSync(translatedFile)) {
                        console.log(`Skipped the translation of ${translatedFile}.`);
                    } else {
                        fs.writeFileSync(translatedFile, result.text, 'utf8', 'w');
                        console.log(`Translated ${args[0]} to ${translatedFile}.`);
                    }
                });
            });
        } else {
            console.error(`${args[0]} is not a valid markdown file.`);
            process.exit(2);
        }
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
})();