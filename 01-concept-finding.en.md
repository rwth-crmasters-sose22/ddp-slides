---
marp: true
---

# DDP-SoSe22 - Class 01
## Mindmap Concept Exploration & Group Activity

<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
</style>

---

# Agenda

- Applications
  - [Miro](https://miro.com)
  - [Draw.io](https://draw.io)
- Concept Tools
  - [Mindmaps](https://en.wikipedia.org/wiki/Mind_map)
- Concept Deliverables
  - Problem space exploration

---

## Bookkeeping

- If you still do not have an RWTH email address:
  - If you do not know what to do:
    - Send an email to Julia at [cr@ip.rwth-aachen.de](mailto:cr@ip.rwth-aachen.de)

- If you have an RWTH email address, but you have no access to either **#moodle-ddp** or **#moodle-dssipd** (please refer to DSSIPD class 01 slides):
  - Send an email to Zhuopeng at [zhuopeng.li@rwth-aachen.de](mailto:zhuopeng.li@rwth-aachen.de).

In either case, please put me in the CC [diraneyya@ip.rwth-aachen.de](mailto:diraneyya@ip.rwth-aachen.de)

---

## Before we start

- <mark>10 out of 28 participants submitted the DSSIPD assignment for tonight ☹️</mark>
- There will be a deduction in the grade for every day that passes after missing the deadline.
- We will demonstrate how to use the `choco` command today to install software

---

## Re-orientation (from Welcoming Day)

> CR Masters's innovation strategy:

  ![](./assets/innovation-strategy.drawio.svg)

> Integrator's workflow:

  ![](./assets/integrators-workflow.drawio.svg)

---

## Applications (Miro)


> Similar to our DSSIPD slides, credentials or logins are referred to using the at `@` symbol, whereas resources are referred to using the `#` symbol.

- [Miro](https://miro.com) is an online platforms for collaborative boards. In order to access the platform, please use the [invitation link here](https://miro.com/welcome/V1hpeWJleHFRbUd0MWh6R0NRNFJ5R0xvVWhjVVh1Q201NXM0MGhpR0Zwc3FIcE9xdzZRa0Z0Q1BYcURkbXdpOXwzMDc0NDU3MzUzMDI5ODQ2NDAx?share_link_id=592676760862) to access the #miro-team by creating @miro logins.
  - To create the logins, please your university email. This is the email that ends with `@rwth-aachen.de`.

---

## Applications (draw.io)

- [Draw.io](https://draw.io) is an online sketching program that is:
  - Very flexible, allowing you to create a variety of diagram types
  - [Open-source](https://github.com/jgraph/drawio), with source-code found on GitHub
  - Have desktop as well as online versions
  - The desktop version can be installed using an installer, or using chocolatey:  

    ```bash
    choco install drawio
    ```
---

## Drawio Examples

Please check the images ending with ".drawio.png" in the `assets` folder in this repo.

---

## Mindmap

<!-- 

@startmindmap
* Big idea
** Smaller idea
*** Even smaller idea
*** ...
*** ...
*** ...
** ...
** ...
** ...
@endmindmap

-->

![height:500px center](./assets/mindmap-basic.svg)

---

## Mindmap (2)

> Start by collecting ideas, complaints, and/or solutions from the market.

![height:400px center](./assets/mindmap-backtracking.svg)

---

## Mindmap (3)

> Work your way backwards from the collected stories, by pinpointing a complete and a mutually-exclusive set of points at each level.

![height:400px center](./assets/mindmap-completeness.svg)

---

## Big idea

_This will lead you to the big idea, the root concept for all of your collected stories._

In the process of working backwards from stories, you discover the all the related problems in a systematic manner (reducing blind spots).

> Now, you have to choose your exact concept from the fuller mindmap to build your POC (i.e. _Proof of Concept_) on top of.

---

## Concept locking

To choose the concept from the fuller mindmap wisely, one needs to take into account:

1. Personal interest
2. Professional interest
3. Research (i.e. _Masters thesis_) interest
4. Open-source software available on GitHub/GitLab (secondary research needed)
5. Time available for the DDP module until the mid colloquium
6. Personal and team skills/competences

---

## What's next (before next class)

1. Use your class contributed concept to enrich the [mindmap we made in class](https://miro.com/app/board/uXjVO9HjnME=/).
2. Contribute your DDP seed concept of interest to the [marketplace Miro board](https://miro.com/app/board/uXjVO9EW93g=/).
3. Put a sticker with your name on concepts proposed by others in the marketplace Miro board above.
