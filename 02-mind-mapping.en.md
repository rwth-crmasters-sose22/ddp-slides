---
marp: true
---

# DDP-SoSe22 - Class 02
## Mind Mapping Exercise (Marketplace concepts)

<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
</style>

---

# Before we begin

- **Excellent submissions to the [gitlab-markdown](https://gitlab.com/rwth-crmasters-sose22/course-work/gitlab-markdown) repository in the DSSIPD class!**
- _Excellent self introductions_: Camilo, Someyyeh, Rodrigo, Sushmitha, Shady, Ramez
- _Good software practices_: Wei
- _Good effort_: Miguel, Robin, Elder, Romero
- <mark>You all did great! 😆</mark>

---

# Improvements

- Thanks to Robin for class feedback!
- For _German_, _Chinese_ and _Spanish_ students: We are working on a translation pipeline for our class slides (still **experimental** at this stage)
- Sharing of screen over LAN for people at the back of the class: [https://192.168.0.178:8443/](https://192.168.0.178:8443/)
- Please use the meeting name "test" for the screen sharing above.
---

# Agenda

- Give feedback on the markdown assignment (DSSIPD class)
- Explore the entries at the [Marketplace Miro board](https://miro.com/app/board/uXjVO9EW93g=/)
- Map some of the marketplace ideas (as well as additional resources) using the [construction process reference mindmap](https://miro.com/app/board/uXjVO7rhzCc=/)
- Install the [starship](https://starship.rs/) prompt for tomorrow's DSSIPD class.

---

## What's next (before next class)

1. Use our discussion in class to get in touch with colleagues with similar interest.
2. Together, start working on the [mind-mapping of the marketplace categories' board](https://miro.com/app/board/uXjVO6zEiz0=/) on Miro.
3. Some concepts, might involve more than one mind-map (e.g. the automation concept).

